(require 'package)
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/"))
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
  (setq package-check-signature nil)

(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(setq use-package-verbose t)
(setq use-package-always-ensure t)

(require 'use-package)

(use-package auto-compile
  :config (auto-compile-on-load-mode))
(setq load-prefer-newer t)

(setq user-full-name "Stan van der Vleuten"
    user-mail-address "Stanvandervleuten95.com")

(use-package winner
  :defer t)

(setq initial-buffer-choice (lambda () (get-buffer "*dashboard*")))

(setq helm-descbinds-window-style 'same-window)

  (setq package-enable-at-startup nil)
  ;(package-initialize)
  (custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
  '(custom-safe-themes
  (quote
  ("8e4efc4bed89c4e67167fdabff77102abeb0b1c203953de1e6ab4d2e3a02939a" default)))
  '(package-selected-packages
  (quote
  (package-build shut-up epl git commander f dash s cask org-bullets evil-org evil-collection gruvbox-theme use-package helm evil-visual-mark-mode))))
  (custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
  '(default ((t (:inherit nil :stipple nil :background "#151515" :foreground "#fdf4c1" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight bold :height 138 :width normal :foundry "CYRE" :family "Inconsolata"))))
  '(fringe ((t (:background "#151515")))))


  (use-package evil
  :ensure t
  :init
(setq evil-want-integration nil)
  :config
  (evil-mode 1))

  (use-package evil-collection
  :after evil
  :ensure t
  :config
  (evil-collection-init))

  (unless (package-installed-p 'use-package)
  (package-refresh-contents)
(package-install 'use-package))

  (eval-when-compile
  (require 'use-package))

  (load-theme 'gruvbox-dark-hard t)

  (require 'helm)
  (require 'helm-config)

  ;; The default "C-x c" is quite close to "C-x C-c", which quits Emacs.
  ;; Changed to "C-c h". Note: We must set "C-c h" globally, because we
  ;; cannot change `helm-command-prefix-key' once `helm-config' is loaded.
  (global-set-key (kbd "C-c h") 'helm-command-prefix)
  (global-unset-key (kbd "C-x c"))

  (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind tab to run persistent action
  (define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB work in terminal
  (define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z

  (when (executable-find "curl")
  (setq helm-google-suggest-use-curl-p t))

  (setq helm-split-window-in-side-p           t ; open helm buffer inside current window, not occupy whole other window
  helm-move-to-line-cycle-in-source     t ; move to end or beginning of source when reaching top or bottom of source.
  helm-ff-search-library-in-sexp        t ; search for library in `require' and `declare-function' sexp.
  helm-scroll-amount                    8 ; scroll 8 lines other window using M-<next>/M-<prior>
  helm-ff-file-name-history-use-recentf t
  helm-echo-input-in-header-line t)

  (defun spacemacs//helm-hide-minibuffer-maybe ()
  "Hide minibuffer in Helm session if we use the header line as input field."
  (when (with-helm-buffer helm-echo-input-in-header-line)
  (let ((ov (make-overlay (point-min) (point-max) nil nil t)))
  (overlay-put ov 'window (selected-window))
  (overlay-put ov 'face
		  (let ((bg-color (face-background 'default nil)))
		  `(:background ,bg-color :foreground ,bg-color)))
  (setq-local cursor-type nil))))


  (add-hook 'helm-minibuffer-set-up-hook
	  'spacemacs//helm-hide-minibuffer-maybe)

  (setq helm-autoresize-max-height 0)
  (setq helm-autoresize-min-height 20)
  (helm-autoresize-mode 1)

  (helm-mode 1)

  (menu-bar-mode -1)
  (tool-bar-mode -1)

  (setq helm-mode-fuzzy-match t)
  (setq helm-completion-in-region-fuzzy-match t)

  (scroll-bar-mode -1)


  (global-set-key (kbd "M-x") 'helm-M-x)

  (helm-autoresize-mode t)


  (require 'org)
  (define-key global-map "\C-cl" 'org-store-link)
  (define-key global-map "\C-ca" 'org-agenda)
  (setq org-log-done t)

  (when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings))

(setq tabbar-background-color "#151515") ;; the color of the tabbar background
(custom-set-faces
'(tabbar-default ((t (:inherit variable-pitch :background "#151515" :foreground "white" :weight bold))))
'(tabbar-button ((t (:inherit tabbar-default :foreground "#151515"))))
'(tabbar-button-highlight ((t (:inherit tabbar-default))))
'(tabbar-highlight ((t (:underline t))))
'(tabbar-selected ((t (:inherit tabbar-default :background "#95CA59"))))
'(tabbar-separator ((t (:inherit tabbar-default :background "#95CA59"))))
	'(tabbar-unselected ((t (:inherit tabbar-default)))))

(global-visual-line-mode t)

(setq evil-insert-state-message nil)
(setq treemacs-no-png-images t)
(setq-default evil-cross-lines t)

(use-package evil-org
:ensure t
:after org
:config
(add-hook 'org-mode-hook 'evil-org-mode)
(add-hook 'evil-org-mode-hook
(lambda ()
(evil-org-set-key-theme))))

(add-hook 'text-mode-hook 'undo-tree-mode) (add-hook 'prog-mode-hook 'undo-tree-mode) (require 'undo-tree) (global-undo-tree-mode) 
(add-hook 'org-mode-hook 'undo-tree-mode)

(setq org-todo-keywords
  '((sequence "TODO(t)" "DOING(:)" "WAITING(w@/!)" "|" "DONE(d!)" "CANCELED(c@)")))

(add-hook 'org-mode-hook 'flyspell-mode)
(add-hook 'org-mode-hook 'guess-language-mode)

(with-eval-after-load 'org (setq org-startup-indented t))
(evil-leader/set-key-for-mode 'org-mode "t" 'org-todo)
(evil-leader/set-key-for-mode 'org-mode "o" 'org-open-at-point)
(evil-leader/set-key-for-mode 'org-mode "e" 'org-export-dispatch)

(setq org-reveal-root "http://cdn.jsdelivr.net/reveal.js/3.0.0/")

(setq ranger-show-hidden nil)

(global-set-key (kbd "C-w") 'backward-kill-word)

(define-key evil-normal-state-map "H" 'beginning-of-line)
(define-key evil-normal-state-map "L" 'end-of-line)
(define-key evil-normal-state-map "u" 'undo-tree-undo)
(define-key evil-normal-state-map "\C-r" 'undo-tree-redo)

(global-evil-leader-mode)
(evil-leader/set-leader ";")
(evil-leader/set-key
"f" 'helm-mini
"q" 'kill-current-buffer
"Q" 'delete-window
"n" 'nlinum-relative-toggle
"r" 'ranger
"g" 'avy-goto-word-1
"<RET>" 'wm-cycle-layout
"d" 'deer
"t" 'neotree-show
";" 'helm-M-x
"'" 'other-window
"v" 'split-window-below
"h" 'split-window-right
"u" 'undo-tree-visualize
"b" 'helm-bookmarks)

(defcustom dashboard-page-separator "\n\n"
  "Separator to use between the different pages."
  :type 'string
  :group 'dashboard)

(setq-default indent-tabs-mode t)
    (require 'powerline-evil)
    (setq powerline-arrow-shape 'utf8)
    (powerline-evil-center-color-theme)

(let ((faces '(mode-line
               mode-line-buffer-id
               mode-line-emphasis
               mode-line-highlight
               mode-line-inactive)))
     (mapc
      (lambda (face) (set-face-attribute face nil :font "Inconsolata-11"))
      faces))

(use-package dashboard
 :config
 (dashboard-setup-startup-hook))
 (setq dashboard-banner-logo-title "Welcome to VIM.. I mean.. EMACS!!")
 (setq dashboard-startup-banner 'logo)
(setq dashboard-items '((recents  . 15)
                     (bookmarks . 5)
                     (agenda . 5)
                     (registers . 5)))

(add-hook 'prog-mode-hook 'highlight-indent-guides-mode)
(setq highlight-indent-guides-method 'fill)

(setq-default tab-width 4)

(global-prettify-symbols-mode t)

(defmacro diminish-minor-mode (filename mode &optional abbrev)
  `(eval-after-load (symbol-name ,filename)
     '(diminish ,mode ,abbrev)))

(defmacro diminish-major-mode (mode-hook abbrev)
  `(add-hook ,mode-hook
             (lambda () (setq mode-name ,abbrev))))

(diminish-minor-mode 'abbrev 'abbrev-mode)
(diminish-minor-mode 'simple 'auto-fill-function)
(diminish-minor-mode 'company 'company-mode)
(diminish-minor-mode 'eldoc 'eldoc-mode)
(diminish-minor-mode 'flycheck 'flycheck-mode)
(diminish-minor-mode 'flyspell 'flyspell-mode)
(diminish-minor-mode 'global-whitespace 'global-whitespace-mode)
(diminish-minor-mode 'org-indent 'org-indent-mode)
(diminish-minor-mode 'projectile 'projectile-mode)
(diminish-minor-mode 'ruby-end 'ruby-end-mode)
(diminish-minor-mode 'subword 'subword-mode)
(diminish-minor-mode 'undo-tree 'undo-tree-mode)
(diminish-minor-mode 'yard-mode 'yard-mode)
(diminish-minor-mode 'yasnippet 'yas-minor-mode)
(diminish-minor-mode 'wrap-region 'wrap-region-mode)

(diminish-minor-mode 'paredit 'paredit-mode " π")

(diminish-major-mode 'emacs-lisp-mode-hook "el")
(diminish-major-mode 'haskell-mode-hook "λ=")
(diminish-major-mode 'lisp-interaction-mode-hook "λ")
(diminish-major-mode 'python-mode-hook "Py")

(use-package nlinum
:config
(nlinum-relative-setup-evil)                    ;; setup for evil
(add-hook 'prog-mode-hook 'nlinum-relative-mode)
(add-hook 'org-mode-hook 'nlinum-relative-mode)
(setq nlinum-relative-current-symbol "")      ;; or "" for display current line number
(setq nlinum-relative-offset 0)
(setq nlinum-relative-redisplay-delay 0))      ;; delay
;; Use `display-line-number-mode` as linum-mode's backend for smooth performance
(setq linum-relative-backend 'display-line-numbers-mode)

(require 'yasnippet)
(yas-global-mode 1)

(add-to-list 'load-path "~/.emacs.d/wm")
(load "wm.el")
(require 'wm)
(wm-mode 1)

(require 'guess-language)
(setq guess-language-languages '(en nl))
(setq guess-language-min-paragraph-length 35)
(setq guess-language-langcodes
  '((en . ("en_GB" "English"))
    (nl . ("nl" "Nederlands"))))

(use-package flycheck
  :hook (prog-mode . flycheck-mode))

(use-package company
  :hook (prog-mode . company-mode)
  :config (setq company-tooltip-align-annotations t)
          (setq company-minimum-prefix-length 1))

(use-package lsp-mode
  :commands lsp
  :config (require 'lsp-clients))

(use-package lsp-ui)

use-package toml-mode)

(use-package rust-mode
  :hook (rust-mode . lsp))

;; Add keybindings for interacting with Cargo
(use-package cargo
  :hook (rust-mode . cargo-minor-mode))

(use-package flycheck-rust
  :config (add-hook 'flycheck-mode-hook #'flycheck-rust-setup))

(defun go-mode-setup ()
  (go-eldoc-setup))
(add-hook 'go-mode-hook 'go-mode-setup)

(defun go-mode-setup ()
 (go-eldoc-setup)
 (setq gofmt-command "~/.go/bin/goimports")
 (add-hook 'before-save-hook 'gofmt-before-save))
(add-hook 'go-mode-hook 'go-mode-setup)

(add-hook 'go-mode-hook (lambda ()
                          (set (make-local-variable 'company-backends) '(company-go))
                          (company-mode)))

;(package-initialize)
(elpy-enable)
(add-hook 'elpy-mode-hook (lambda () (highlight-indentation-mode -1)))

(require 'js2-mode)
(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
(add-hook 'js2-mode-hook #'js2-refactor-mode)
(add-hook 'js2-mode-hook #'setup-tide-mode)


(defun setup-tide-mode ()
  (interactive)
  (tide-setup)
  (flycheck-mode +1)
  (setq flycheck-check-syntax-automatically '(save mode-enabled))
  (eldoc-mode +1)
  (tide-hl-identifier-mode +1)
  (company-mode +1))

;; aligns annotation to the right hand side
(setq company-tooltip-align-annotations t)

;; formats the buffer before saving
(add-hook 'before-save-hook 'tide-format-before-save)

(add-hook 'typescript-mode-hook #'setup-tide-mode)

(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.jsx\\'" . web-mode))
(add-hook 'web-mode-hook
          (lambda ()
            (when (string-equal "jsx" (file-name-extension buffer-file-name))
              (setup-tide-mode))))
              (require 'flycheck)
(flycheck-add-mode 'javascript-eslint 'web-mode)

(require 'prettier-js)
(add-hook 'js2-mode-hook 'prettier-js-mode)
(setq prettier-js-args '(
  "--trailing-comma" "all"
  "--bracket-spacing" "false"
))

;; MARKDOWN
(add-to-list 'auto-mode-alist '("\\.md" . poly-markdown-mode))
;;; R modes
(add-to-list 'auto-mode-alist '("\\.Snw" . poly-noweb+r-mode))
(add-to-list 'auto-mode-alist '("\\.Rnw" . poly-noweb+r-mode))
(add-to-list 'auto-mode-alist '("\\.Rmd" . poly-markdown+r-mode))

(setq TeX-parse-self t)

(setq TeX-PDF-mode t)

(setq org-export-headline-levels 7)
