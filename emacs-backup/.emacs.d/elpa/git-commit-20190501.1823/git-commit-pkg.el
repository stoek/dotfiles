;;; -*- no-byte-compile: t -*-
(define-package "git-commit" "20190501.1823" "Edit Git commit messages" '((emacs "25.1") (dash "20180910") (with-editor "20181103")) :commit "251d869095fcad7e9eecb8e47bcd7b315ae9acee" :keywords '("git" "tools" "vc") :maintainer '("Jonas Bernoulli" . "jonas@bernoul.li") :url "https://github.com/magit/magit")
