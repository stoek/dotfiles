(require 'package)
          (add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))
          (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
          (add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/"))
        (setq package-check-signature nil)
      (package-initialize)
  (unless package-archive-contents
    (package-refresh-contents))

    ;; Initialize use-package on non-Linux platforms
  (unless (package-installed-p 'use-package)
    (package-install 'use-package))

  (require 'use-package)
  (setq use-package-always-ensure t)
(add-to-list 'load-path "~/.emacs.d/")

(use-package auto-package-update
  :custom
  (auto-package-update-interval 7)
  (auto-package-update-prompt-before-update t)
  (auto-package-update-hide-results t)
  :config
  (auto-package-update-maybe)
  (auto-package-update-at-time "09:00"))

;; NOTE: If you want to move everything out of the ~/.emacs.d folder
;; reliably, set `user-emacs-directory` before loading no-littering!
(setq user-emacs-directory "~/.cache/emacs")

(use-package no-littering)

;; no-littering doesn't set this by default so we must place
;; auto save files in the same path as it uses for sessions
(setq auto-save-file-name-transforms
      `((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))

(use-package gruvbox-theme :ensure t)
(load-theme 'gruvbox-dark-hard t)

(setq inhibit-startup-message t)

(scroll-bar-mode -1)        ; Disable visible scrollbar
(tool-bar-mode -1)          ; Disable the toolbar
(tooltip-mode -1)           ; Disable tooltips
(set-fringe-mode 10)        ; Give some breathing room

(menu-bar-mode -1)            ; Disable the menu bar

;; Set up the visible bell
(setq visible-bell t)

(defvar efs/default-font-size 130)
(defvar efs/default-variable-font-size 130)
;; 
(set-face-attribute 'default nil :font "Inconsolata" :weight 'bold :height efs/default-font-size)

;; Set the fixed pitch face
(set-face-attribute 'fixed-pitch nil :font "Inconsolata" :weight 'bold :height efs/default-font-size)

;; Set the variable pitch face
(set-face-attribute 'variable-pitch nil :font "Inconsolata" :weight 'bold :height efs/default-variable-font-size)

(use-package all-the-icons)

(use-package doom-modeline
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 12)))

;; Make ESC quit prompts
  (global-set-key (kbd "<escape>") 'keyboard-escape-quit)

  (use-package general
    :after evil
    :config
    (general-create-definer efs/leader-keys
      :keymaps '(normal insert visual emacs)
      :prefix "SPC"
      :global-prefix "C-SPC")

    (efs/leader-keys
      "x" 'execute-extended-command	
      "d" 'deer
      "spc" '(lambda () (interactive) (flyspell-correct-wrapper))
      "config" '(lambda () (interactive) (find-file (expand-file-name "~/.emacs.d/config.org")))))

  (use-package evil
    :init
    (setq evil-want-integration t)
    (setq evil-want-keybinding nil)
    (setq evil-want-C-u-scroll t)
    (setq evil-want-C-i-jump nil)
    :config
    (evil-mode 1)
    (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
    (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)

    ;; Use visual line motions even outside of visual-line-mode buffers
    (evil-global-set-key 'motion "j" 'evil-next-visual-line)
    (evil-global-set-key 'motion "k" 'evil-previous-visual-line)

    (evil-set-initial-state 'messages-buffer-mode 'normal)
    (evil-set-initial-state 'dashboard-mode 'normal))

  (use-package evil-collection
    :after evil
    :config
    (evil-collection-init))

(define-key evil-normal-state-map "H" 'beginning-of-line)
(define-key evil-normal-state-map "L" 'end-of-line)

;; (use-package ivy
  ;; :diminish
  ;; :bind (("C-s" . swiper)
         ;; :map ivy-minibuffer-map
         ;; ("TAB" . ivy-alt-done)
         ;; ("C-l" . ivy-alt-done)
         ;; ("C-j" . ivy-next-line)
         ;; ("C-k" . ivy-previous-line)
         ;; :map ivy-switch-buffer-map
         ;; ("C-k" . ivy-previous-line)
         ;; ("C-l" . ivy-done)
         ;; ("C-d" . ivy-switch-buffer-kill)
         ;; :map ivy-reverse-i-search-map
         ;; ("C-k" . ivy-previous-line)
         ;; ("C-d" . ivy-reverse-i-search-kill))
  ;; :config
  ;; (ivy-mode 1))
;; (setq ivy-initial-inputs-alist ())

;; (use-package counsel
  ;; :bind (("C-M-j" . 'counsel-switch-buffer)
         ;; :map minibuffer-local-map
         ;; ("C-r" . 'counsel-minibuffer-history))
  ;; :custom
  ;; (counsel-linux-app-format-function #'counsel-linux-app-format-function-name-only)
  ;; :config
  ;; (counsel-mode 1))

(use-package vertico
      ;; :straight '(vertico :host github
      ;;                     :repo "minad/vertico"
      ;;                     :branch "main")
      :bind (:map vertico-map
             ("M-j" . vertico-next)
             ("M-k" . vertico-previous)
             ("C-f" . vertico-exit)
             ("M-/" . vertico-insert)
             :map minibuffer-local-map
             ("M-h" . dw/minibuffer-backward-kill))
      :custom
      (vertico-cycle t)
      :custom-face
      (vertico-current ((t (:background "#3a3f5a"))))
      :init
      (vertico-mode))

  (load "vertico-quick.el")
  (load "vertico-indexed.el")
;; Use the `orderless' completion style. Additionally enable
;; `partial-completion' for file path expansion. `partial-completion' is
;; important for wildcard support. Multiple files can be opened at once
;; with `find-file' if you enter a wildcard. You may also give the
;; `initials' completion style a try.
(use-package orderless
  :init
  (setq completion-styles '(orderless)
        completion-category-defaults nil
        completion-category-overrides '((file (styles partial-completion)))))

(use-package ivy-prescient
  :after counsel
  :custom
  (ivy-prescient-enable-filtering nil)
  :config
  ;; Uncomment the following line to have sorting remembered across sessions!
  ;(prescient-persist-mode 1)
  (ivy-prescient-mode 1))

(use-package avy
  :commands (avy-goto-char avy-goto-word-0 avy-goto-line)
  :config
  (setq avy-background nil))



(efs/leader-keys
  "j"   '(:ignore t :which-key "jump")
  "jj"  '(avy-goto-char :which-key "jump to char")
  "jw"  '(avy-goto-word-0 :which-key "jump to word")
  "jl"  '(avy-goto-line :which-key "jump to line"))

(use-package dashboard
:config
(dashboard-setup-startup-hook))
;; (setq dashboard-banner-logo-title "Welcome to VIM.. I mean.. EMACS!!")
(setq dashboard-startup-banner 'logo)
(setq dashboard-items '((recents  . 15)
                    (bookmarks . 5)
                    (registers . 5)))

;; (use-package dired
      ;; :ensure nil
      ;; :commands (dired dired-jump)
      ;; :bind (("C-x C-j" . dired-jump))
      ;; :custom ((dired-listing-switches "-agho --group-directories-first"))
      ;; :config
      ;; (evil-collection-define-key 'normal 'dired-mode-map
	;; "h" 'dired-single-up-directory
	;; "l" 'dired-single-buffer))
(use-package ranger)
(ranger-override-dired-mode t)
(setq ranger-show-hidden t)

(use-package evil-nerd-commenter
  :bind ("M-/" . evilnc-comment-or-uncomment-lines))

(use-package org-bullets
:ensure t
:init
(setq org-bullets-bullet-list
'("*" "+" "-" "*" "+" "-"))
:config
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

(defun efs/org-mode-visual-fill ()
(setq visual-fill-column-width 100
     visual-fill-column-center-text t)
(visual-fill-column-mode 1))

(use-package visual-fill-column
 :hook (org-mode . efs/org-mode-visual-fill))
(add-hook 'org-mode-hook
          (lambda ()
           (org-indent-mode t)
          (visual-line-mode t))
       t)

(with-eval-after-load 'org
;; This is needed as of Org 9.2
(require 'org-tempo)

(add-to-list 'org-structure-template-alist '("sh" . "src shell"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("py" . "src python")))

(add-hook 'org-mode-hook 'flyspell-mode)
          (add-hook 'org-mode-hook 'guess-language-mode)
     (use-package guess-language)
     (setq guess-language-languages '(en nl))
     (setq guess-language-min-paragraph-length 35)
     (setq guess-language-langcodes
       '((en . ("en_GB" "English"))
         (nl . ("nl" "Nederlands"))))
(use-package flyspell-correct
  :after flyspell
  :bind (:map flyspell-mode-map ("C-;" . flyspell-correct-wrapper)))

(setq org-hide-emphasis-markers t)
