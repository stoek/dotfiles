(define-package "evil-collection" "20210816.1010" "A set of keybindings for Evil mode"
  '((emacs "25.1")
    (evil "1.2.13")
    (annalist "1.0"))
  :commit "3d990ce63e07c05a074b52125ac504b671f9f93a" :authors
  '(("James Nguyen" . "james@jojojames.com"))
  :maintainer
  '("James Nguyen" . "james@jojojames.com")
  :keywords
  '("evil" "tools")
  :url "https://github.com/emacs-evil/evil-collection")
;; Local Variables:
;; no-byte-compile: t
;; End:
