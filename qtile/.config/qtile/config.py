from libqtile.config import EzKey, Key, Screen, Group, Drag, Click
# from libqtile.lazy import lazy
from libqtile import layout, bar, widget, hook
import subprocess
from libqtile.command import lazy


from typing import List  # noqa: F401
from libqtile.widget import base


from Xlib import display as xdisplay


def get_num_monitors():
    num_monitors = 0
    try:
        display = xdisplay.Display()
        screen = display.screen()
        resources = screen.root.xrandr_get_screen_resources()

        for output in resources.outputs:
            monitor = display.xrandr_get_output_info(output, resources.config_timestamp)
            preferred = False
            if hasattr(monitor, "preferred"):
                preferred = monitor.preferred
            elif hasattr(monitor, "num_preferred"):
                preferred = monitor.num_preferred
            if preferred:
                num_monitors += 1
    except Exception as e:
        # always setup at least one monitor
        return 1
    else:
        return num_monitors

num_monitors = get_num_monitors()

mod = "mod4"

keys = [
    EzKey(k[0], *k[1:])
    for k in [
        ("M-h", lazy.layout.shrink()),
        ("M-j", lazy.layout.down()),      
        ("M-S-j", lazy.layout.shuffle_down()),
        ("M-S-q", lazy.shutdown()),
        ("M-k", lazy.layout.up()),       
        ("M-S-k", lazy.layout.shuffle_up()),
        ("M-l", lazy.layout.grow()),
        ("M-r", lazy.restart()),
        ("M-q", lazy.window.kill()),
        ("M-<Return>", lazy.spawn("alacritty")),
        ("M-b", lazy.spawn("bartog")),
        ("M-d", lazy.spawn("launch")),
        ("M-<Tab>", lazy.next_layout()),
    ]
]


def init_group_names():
    return [
        ("home", {"layout": "monadtall"}),
        ("term", {"layout": "monadtall"}),
        ("code", {"layout": "monadtall"}),
        ("file", {"layout": "monadtall"}),
        ("surf", {"layout": "monadtall"}),
        ("docs", {"layout": "monadtall"}),
        ("chat", {"layout": "monadtall"}),
        ("temp", {"layout": "monadtall"}),
        ("musc", {"layout": "monadtall"}),
        ("xtra", {"layout": "monadtall"}),
    ]


def init_groups():
    return [Group(name, **kwargs) for name, kwargs in group_names]

group_names = init_group_names()
groups = init_groups()

# def go_to_group(group, i):
#     def f(qtile):
#         if group in '123456789' and qtile.screens.index(qtile.current_screen) == 0:
#             qtile.focus_screen(0)
#             lazy.group[group].toscreen()
#     return f

def go_to_mon():
    def f(qtile):
        qtile.focus_screen(1)
        qtile.focus_screen(0)
        lazy.group["xtra"].toscreen(1)
    return f

def go_to_group2():
    def f(qtile):
        qtile.focus_screen(1)
        lazy.group["xtra"].toscreen(1)
    return f

for i, (name, kwargs) in enumerate(group_names, 1):
        # keys.append(Key([mod], str(i), lazy.function(go_to_group(name))))
        if name != "xtra":
            keys.append(Key([mod], str(i), lazy.group[name].toscreen(0, toggle=False)))
            keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name)))


# keys.append(Key([mod], str(0), lazy.function(go_to_group(name, i)))),
keys.append(Key([mod], str('e'), lazy.function(go_to_group2()))),
keys.append(Key([mod, "shift"], str('e'), lazy.window.togroup("xtra"))),
keys.append(Key([mod], str('m'), lazy.function(go_to_mon()))),
keys.append(Key([mod], "space", lazy.next_layout())),
keys.append(Key([mod], "f", lazy.window.toggle_fullscreen())),



layouts = [
    # layout.Max(margin=10),
    layout.Stack(border_normal="#252525", border_focus="#303030", border_width=6,num_stacks=1, margin=10),
    # Try more layouts by unleashing below layouts.
    # layout.Bsp(),
    # layout.Columns(num_columns=1, border),
    # layout.Matrix(),
    layout.MonadTall(
        border_normal="#252525",
        border_focus="#303030",
        border_width=6,
        margin=10,
        ratio=0.5,
    ),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(margin=10, columnwidth=10),
]


widget_defaults = dict(
    font='Alata',
    fontsize=22,
    padding=3,
)

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.TextBox(
			        text = "vpn:",
                    foreground="#665c54",
		            ),
                widget.GenPollText(update_interval=10, func=lambda: subprocess.check_output("checkvpn").decode("utf-8"), foreground="#d3869b"),
                widget.TextBox(
			        text = "ip:",
                    foreground="#665c54",
		            ),
                widget.GenPollText(update_interval=10, func=lambda: subprocess.check_output("getip").decode("utf-8"), foreground="#83a598"),
                widget.TextBox(
			        text = "wifi:",
                    foreground="#665c54",
		            ),
                widget.Wlan(
                    interface="wlp1s0", 
                    foreground="#b8bb26",
                    format="{essid}"
                ),
                widget.Spacer(),
                widget.GroupBox(
                   # border_width=2,
                    disable_drag=True,
                    highlight_method="text",
                    active="#7c6f64",
                    visible_groups=['home', 'term', 'code', 'file', 'surf', 'docs', 'chat', 'temp', 'musc'],
                    this_current_screen_border="#ebdbb2",
                    #highlight_color=['000000', '282828'],
                    inactive="#3c3836",
                    spacing=0,
                ),
                widget.Spacer(),
                widget.GenPollText(update_interval=10, func=lambda: subprocess.check_output("acorbat").decode("utf-8"), foreground="#665c54"),
                widget.GenPollText(update_interval=10, func=lambda: subprocess.check_output("bat1").decode("utf-8"), foreground="#fabd2f"),
                widget.TextBox(
			        text = "vol:",
                    foreground="#665c54",
		            ),
                widget.GenPollText(update_interval=10, func=lambda: subprocess.check_output("checkvol").decode("utf-8"), foreground="#e67e22"),
                widget.TextBox(
			        text = "time:",
                    foreground="#665c54",
		            ),
                widget.Clock(
                    format='%H:%M',
                    foreground="#fb4934"
                ),
            ],
            30,
            background="#202020",
            margin=[5,30,0,30]
        ),
    )
]

if num_monitors > 1:


    widget_defaults = dict(
        font='Alata',
        fontsize=16,
        padding=3,
        )
    screens = [
    Screen(
        top=bar.Bar(
            [
                widget.TextBox(
			        text = "vpn:",
                    foreground="#665c54",
		            ),
                widget.GenPollText(update_interval=10, func=lambda: subprocess.check_output("checkvpn").decode("utf-8"), foreground="#d3869b"),
                widget.TextBox(
			        text = "ip:",
                    foreground="#665c54",
		            ),
                widget.GenPollText(update_interval=10, func=lambda: subprocess.check_output("getip").decode("utf-8"), foreground="#83a598"),
                widget.TextBox(
			        text = "wifi:",
                    foreground="#665c54",
		            ),
                widget.Wlan(
                    interface="wlp1s0", 
                    foreground="#b8bb26",
                    format="{essid}"
                ),
                widget.Spacer(),
                widget.GroupBox(
                   # border_width=2,
                    disable_drag=True,
                    highlight_method="text",
                    active="#7c6f64",
                    visible_groups=['home', 'term', 'code', 'file', 'surf', 'docs', 'chat', 'temp', 'musc'],
                    this_current_screen_border="#ebdbb2",
                    #highlight_color=['000000', '282828'],
                    inactive="#3c3836",
                    spacing=0,
                ),
                widget.Spacer(),
                widget.GenPollText(update_interval=10, func=lambda: subprocess.check_output("acorbat").decode("utf-8"), foreground="#665c54"),
                widget.GenPollText(update_interval=10, func=lambda: subprocess.check_output("bat1").decode("utf-8"), foreground="#fabd2f"),
                widget.TextBox(
			        text = "vol:",
                    foreground="#665c54",
		            ),
                widget.GenPollText(update_interval=10, func=lambda: subprocess.check_output("checkvol").decode("utf-8"), foreground="#e67e22"),
                widget.TextBox(
			        text = "time:",
                    foreground="#665c54",
		            ),
                widget.Clock(
                    format='%H:%M',
                    foreground="#fb4934"
                ),
            ],
            30,
            background="#202020",
            margin=[5,30,0,30]
        ),
    ), 
      Screen(
        # top=bar.Bar(
        #     [
        #         widget.Spacer(),
        #         widget.GroupBox(
        #            # border_width=2,
        #             disable_drag=True,
        #             highlight_method="text",
        #             active="#7c6f64",
        #             visible_groups=['xtra'],
        #             this_current_screen_border="#ebdbb2",
        #             #highlight_color=['000000', '282828'],
        #             inactive="#3c3836",
        #             spacing=0,
        #             fontsize=27
        #         ),
        #         widget.Spacer(),
        #     ],
        #     30,
        #     background="#202020",
        #     margin=[5,30,0,30]
        # ),
        

    )
]


# Drag floating layouts.
# mouse = [
#    Drag(
#        [mod],
#        "Button1",
#        lazy.window.set_position_floating(),
#        start=lazy.window.get_position(),
#    ),
#    Drag(
#        [mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()
#    ),
#    Click([mod], "Button2", lazy.window.bring_to_front()),
# ]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        {"wmclass": "confirm"},
        {"wmclass": "dialog"},
        {"wmclass": "download"},
        {"wmclass": "error"},
        {"wmclass": "file_progress"},
        {"wmclass": "notification"},
        {"wmclass": "splash"},
        {"wmclass": "toolbar"},
        {"wmclass": "confirmreset"},  # gitk
        {"wmclass": "makebranch"},  # gitk
        {"wmclass": "maketag"},  # gitk
        {"wname": "branchdialog"},  # gitk
        {"wname": "pinentry"},  # GPG key password entry
        {"wmclass": "ssh-askpass"},  # ssh-askpass
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"


@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])


# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
